﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="18008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="class_snake.lvclass" Type="LVClass" URL="../class_snake.lvclass"/>
		<Item Name="draw_snake.vi" Type="VI" URL="../draw_snake.vi"/>
		<Item Name="global_direction.vi" Type="VI" URL="../global_direction.vi"/>
		<Item Name="global_length.vi" Type="VI" URL="../global_length.vi"/>
		<Item Name="global_snake.vi" Type="VI" URL="../global_snake.vi"/>
		<Item Name="main.vi" Type="VI" URL="../main.vi"/>
		<Item Name="type_coordinate.ctl" Type="VI" URL="../type_coordinate.ctl"/>
		<Item Name="type_direction.ctl" Type="VI" URL="../type_direction.ctl"/>
		<Item Name="type_RW.ctl" Type="VI" URL="../type_RW.ctl"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Draw Rectangle.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Rectangle.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="Move Pen.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Move Pen.vi"/>
				<Item Name="Set Pen State.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Set Pen State.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
